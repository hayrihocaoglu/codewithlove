#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    InitializeObjects();
    setFixedSize(SCREEN_WIDTH,SCREEN_HEIGHT);
}

MainWindow::~MainWindow()
{

}

void MainWindow::InitializeObjects()
{
}
void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        x = event->x();
        y = event->y();
        repaint();
    }
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    painter = new QPainter(this);
    painter->setPen(QPen(Qt::black,1));
    painter->drawRect(x,y,30,30);
    delete painter;
}
