
#include "List.h"

template<class T>
List<T>::List() : EObject()
{
	count = 0;
	startPtr = NULL;
	endPtr = NULL;
}

template<class T>
List<T>::~List()
{
	
}
template<class T>
List<T>* List<T>::clone()
{
    List<T>* list = new List<T>();
    Node<T>* crnPtr = startPtr;
    if(crnPtr != NULL)
    {
        do
        {
            list->addItem(crnPtr->clone());
            crnPtr = crnPtr->getNextPtr();
        }while(crnPtr != startPtr);
    }
    return list;
}
template<class T>
EString* List<T>::toString()
{
    Node<T>* crnPtr = startPtr;
    Node<T>* nxtPtr;
    EString* str = new EString((UINT8_e*)"");
    do
    {
        str->concat(crnPtr->toString());
        str->concat((UINT8_e*)"\n");
        nxtPtr = crnPtr->getNextPtr();
    }while(nxtPtr != startPtr);
    return str;
}
template<class T>
bool List<T>::isEmpty()
{
	if(startPtr == NULL)
	{
		return true;
	}
	return false;
}

template<class T>
void List<T>::addItem(T* item)
{
	Node<T>* newPtr = new Node<T>(item);
	if(isEmpty() == true)
	{
		newPtr->setIndex(count);
		startPtr = newPtr;
		endPtr = newPtr;
		newPtr->setNextPtr(endPtr);
		newPtr->setPrevPtr(startPtr);
		count++;
	}
	else
	{
		newPtr->setIndex(count);
		newPtr->setPrevPtr(endPtr);
		endPtr->setNextPtr(newPtr);
		endPtr = newPtr;
		startPtr->setPrevPtr(endPtr);
		endPtr->setNextPtr(startPtr);
		count++;
	}
}

template<class T>
void List<T>::removeItem(T* item)
{
	Node<T>* crnPtr = startPtr;
	if(containItem(item,crnPtr))
	{
		if(crnPtr == startPtr)
		{
			crnPtr->getNextPtr()->setPrevPtr(endPtr);
			endPtr->setNextPtr(crnPtr->getNextPtr());
			startPtr = crnPtr->getNextPtr();
			delete crnPtr;
		}
		else if(crnPtr == endPtr)
		{
			crnPtr->getPrevPtr()->setNextPtr(startPtr);
			startPtr->setPrevPtr(crnPtr->getPrevPtr());
			endPtr = crnPtr->getPrevPtr();
			delete crnPtr;
		}
		else
		{
			crnPtr->getPrevPtr()->setNextPtr(crnPtr->getNextPtr());
			crnPtr->getNextPtr()->setPrevPtr(crnPtr->getPrevPtr());
			delete crnPtr;
		}
		count--;
	}
	
}

template<class T>
bool List<T>::containItem(T* item,Node<T>* crnPtr)
{
	bool contain = false;
	if(!isEmpty())
	{
		if(count == 1)
		{
			contain = (crnPtr->getData() ==  item);
		}
		else
		{
			do
			{
				if(crnPtr->getData() == item)
				{
					contain = true;
					break;
				}
				crnPtr = crnPtr->getNextPtr();
			}while(crnPtr->getNextPtr() != startPtr);
		}
	}
	return contain;
}

template<class T>
bool List<T>::containItem(T* item)
{
	bool contain = false;
	Node<T>* crnPtr = startPtr;
	if(!isEmpty())
	{
		if(count == 1)
		{
			contain = (crnPtr->getData() ==  item);
		}
		else
		{
			do
			{
				if(crnPtr->getData() == item)
				{
					contain = true;
					break;
				}
				crnPtr = crnPtr->getNextPtr();
			}while(crnPtr->getNextPtr() != startPtr);
		}
	}
	return contain;
}
template<class T>
void List<T>::clear()
{
	Node<T>* crnPtr = startPtr;
	Node<T>* nxtPtr;
	if(!isEmpty())
	{
		if(count == 1)
		{
			count = 0;
			delete crnPtr;
		}
		else
		{
			do
			{				
				nxtPtr = crnPtr->getNextPtr();
				delete crnPtr;
			}while(nxtPtr != startPtr);
			count = 0;
		}
	}
}

template<class T>
T* List<T>::getItem(uint32_t _index)
{
	Node<T>* crnPtr = startPtr;
	if((_index >= count))
	{
		return NULL;
	}
	else
	{
		for(int i=0;i <= _index;i++)
		{
			crnPtr = crnPtr->getNextPtr();
		}
	}
	return crnPtr->getData();
}
template<class T>
uint32_t List<T>::size()
{
	return count;
}

