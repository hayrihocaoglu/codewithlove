#ifndef __STRING_H__
#define __STRING_H__

#include "stdint.h"
#include "stdio.h"
#include "lib/edefinition.h"
class EString
{
public:
    EString(UINT8_e* _str);
    EString(UINT32_e val);
    ~EString();

    BOOL_e IsEmpty();
    BOOL_e IsEqual(EString* _str);

    BOOL_e contains(UINT8_e val);
    BOOL_e contains(EString* _str);
    BOOL_e contains(UINT8_e* _char);

    BOOL_e endsWith(UINT8_e val);
    BOOL_e endsWith(EString* _str);
    BOOL_e endsWith(UINT8_e* _char);

    BOOL_e startsWith(UINT8_e val);
    BOOL_e startsWith(EString* _str);
    BOOL_e startsWith(UINT8_e* _char);

    EString* copy();
    EString* copy(UINT8_e offset,UINT8_e length);
    static EString* copy(UINT8_e* _char,UINT8_e offset,UINT8_e length);

    EString* subString(UINT8_e beginIndex);
    EString* subString(UINT8_e beginIndex,UINT8_e endIndex);

    UINT8_e charAt(UINT8_e index);

    void concat(EString* _str);
    void concat(UINT8_e* _str);
    void concat(UINT32_e val);

    UINT8_e indexOf(UINT8_e _char);

    UINT8_e* GetString();
    UINT8_e GetSize();

public:
    static const UINT8_e MAX_SIZE = 100;

private:
    UINT8_e str[MAX_SIZE];
    UINT8_e	length;

};
#endif
