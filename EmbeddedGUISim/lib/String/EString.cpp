
#include "lib/String/EString.h"

EString::EString(UINT8_e* _str)
{
	length = 0;
	for(length = 0;length < MAX_SIZE;length++)
	{
		str[length] = _str[length];
		if(str[length] == '\0')
		{
			break;
		}
	}
}
EString::EString(UINT32_e val)
{
    sprintf((char*)str,"%d",val);
}

EString* EString::copy(UINT8_e* _char,UINT8_e offset,UINT8_e _length)
{
    UINT8_e _str[MAX_SIZE];
    UINT8_e i = 0;
	while(_char[offset + i] != '\0')
	{
		if(offset + i < MAX_SIZE)
		{
			_str[i] = _char[offset + i];
			i++;
		}
		else
		{
			break;
		}
		if(i >= _length)
		{
				break;
		}
	}
    return new EString(_str);
}

UINT8_e EString::GetSize()
{
	return length;
}

BOOL_e EString::IsEmpty()
{
	if(length == 0)
	{
        return TRUE_e;
	}
    return FALSE_e;
}

BOOL_e EString::IsEqual(EString* _str)
{
	int i = 0;
	while(charAt(i) == _str->charAt(i))
	{
		if(charAt(i) == '\0' && _str->charAt(i) == '\0')
		{
            return TRUE_e;
		}
		i++;
	}
    return FALSE_e;
}

UINT8_e EString::charAt(UINT8_e index)
{
	if(index > length)
    {
		return index;
	}
	return str[index];
}

UINT8_e* EString::GetString()
{
	return str;
}

UINT8_e EString::indexOf(UINT8_e _char)
{
	int i = 0;
	while(charAt(i) != _char)
	{
		if(i >= length)
        {
			break;
		}
		i++;
	}
	return i;
}

void EString::concat(EString* _str)
{
	int tmpLength = length;
	if((length + _str->GetSize()) < MAX_SIZE)
	{	
		for(int i = 0; i < _str->GetSize(); i++)
		{
			str[tmpLength + i] = _str->charAt(i);
			length++;
		}
	}	
}

void EString::concat(UINT8_e* _str)
{
	int tmpLength = length;
	int i = 0;
	while(_str[i] != '\0')
	{
		str[tmpLength + i] = _str[i];
		length++;
		i++;
		if(length >=MAX_SIZE)
		{
			break;
		}
	}		
}
void EString::concat(UINT32_e val)
{
    sprintf((char*)str,"%d",val);
}
BOOL_e EString::contains(UINT8_e val)
{
    UINT8_e i = 0;
	while(str[i] != val)
	{
		if(i >= length)
		{
            return FALSE_e;
		}
		i++;
	}
    return TRUE_e;
}
BOOL_e EString::contains(EString* _str)
{
    UINT8_e i = 0;
    UINT8_e j = 0;
	while(str[i] != '\0')
	{
		if(str[i] == _str->charAt(j))
		{
			j++;
		}
		else
		{
			j = 0;
		}
		if(j >= _str->GetSize())
		{
            return TRUE_e;
		}
		i++;
	}
    return FALSE_e;
}
BOOL_e EString::contains(UINT8_e* _char)
{
    UINT8_e i = 0;
    UINT8_e j = 0;
	while(str[i] != '\0')
	{
		if(str[i] == _char[j])
		{
			j++;
		}
		else
		{
			j = 0;
		}
		if(_char[j] == '\0')
		{
            return TRUE_e;
		}
		i++;
	}
    return FALSE_e;
}

BOOL_e EString::startsWith(UINT8_e val)
{
	if(str[0] == val)
	{
        return TRUE_e;
	}
    return FALSE_e;
}
BOOL_e EString::startsWith(EString* _str)
{
    UINT8_e i = 0;
	while(str[i] == _str->charAt(i))
	{
		i++;
		if(_str->charAt(i) == '\0')
		{
            return TRUE_e;
		}
	}
    return FALSE_e;
}
BOOL_e EString::startsWith(UINT8_e* _char)
{
    UINT8_e i = 0;
	while(str[i] == _char[i])
	{
		i++;
		if(_char[i] == '\0')
		{
            return TRUE_e;
		}
	}
    return FALSE_e;
}

BOOL_e EString::endsWith(UINT8_e val)
{
	if(str[length - 1] == val)
	{
        return TRUE_e;
	}
    return FALSE_e;
}

BOOL_e EString::endsWith(EString* _str)
{
	int8_t i = length - 1;
	int8_t j = _str->GetSize() - 1;
	while(str[i] == _str->charAt(j))
	{
		i--;
		j--;
		if(j < 0)
		{
            return TRUE_e;
		}
	}
    return FALSE_e;
}
BOOL_e EString::endsWith(UINT8_e* _char)
{
	int8_t i = length - 1;
	int8_t j = 0;
	for(j = 0;j < MAX_SIZE;j++)
	{
		if(_char[j] == '\0')
		{
			j--;
			break;
		}
	}
	while(str[i] == _char[j])
	{
		i--;
		j--;
		if(j < 0)
		{
            return TRUE_e;
		}
	}
    return FALSE_e;
}

EString* EString::copy()
{
    return new EString(str);
}
EString* EString::copy(UINT8_e offset,UINT8_e _length)
{
    UINT8_e _str[MAX_SIZE];
	for(int i=0;i < _length;i++)
	{
		if(offset + i < length)
		{
			_str[i] = str[offset + i];
		}
		else
		{
			break;
		}
	}
    return new EString(_str);
}

EString* EString::subString(UINT8_e beginIndex)
{
    UINT8_e _str[MAX_SIZE];
	for(int i = 0;i< length;i++)
	{
		if(beginIndex + i <= length)
		{
			_str[i] = str[beginIndex + i];			
		}
		else
		{
			break;
		}
	}
    return new EString(_str);
}

EString* EString::subString(UINT8_e beginIndex,UINT8_e endIndex)
{
    UINT8_e _str[MAX_SIZE];
	for(int i = 0;i< length;i++)
	{
		if(beginIndex + i <= length)
		{
			_str[i] = str[beginIndex + i];			
		}
		else
		{
			break;
		}
		if(beginIndex + i == endIndex)
		{
			break;
		}
	}
    return new EString(_str);
}



































