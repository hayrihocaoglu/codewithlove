#ifndef EDEFINITION_H
#define EDEFINITION_H

typedef unsigned char UINT8_e;
typedef unsigned short UINT16_e;
typedef unsigned int   UINT32_e;

typedef signed char  INT8_e;
typedef short INT16_e;
typedef int   INT32_e;

typedef bool BOOL_e;

typedef struct EPoint
{
    UINT32_e x;
    UINT32_e y;
    EPoint(UINT32_e _x,UINT32_e _y)
    {
        x = _x;
        y = _y;
    }
}EPoint;

#define FALSE_e 0
#define TRUE_e 1

#endif // EDEFINITION_H
