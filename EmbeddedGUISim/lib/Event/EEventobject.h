#ifndef EEVENTOBJECT_H
#define EEVENTOBJECT_H

#include "lib/EObject.h"
#include "lib/String/EString.h"

class EEventObject : public EObject
{
public:
    EEventObject* clone();

public:
    EEventObject(EObject* src,UINT32_e ID,UINT8_e* cmd);

    EObject* getSource();
    void setSource(EObject* src);
    EString* toString();
    UINT32_e getID();
private:

protected:
    EObject* source;
    EString* command;
    UINT32_e id;
};

#endif // EEVENTOBJECT_H
