#include "lib/Event/EEventobject.h"

EEventObject::EEventObject(EObject* src,UINT32_e ID,UINT8_e* cmd) : EObject()
{
    source = src;
    id = ID;
    command = new EString(cmd);
}

EEventObject* EEventObject::clone()
{
    return new EEventObject(source,id,command->GetString());
}
EObject* EEventObject::getSource()
{
    return source;
}
void EEventObject::setSource(EObject* src)
{
    source = src;
}
EString* EEventObject::toString()
{
    return command;
}
UINT32_e EEventObject::getID()
{
    return id;
}
