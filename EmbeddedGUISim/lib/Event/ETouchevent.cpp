#include "ETouchevent.h"

ETouchEvent::ETouchEvent(EObject* src,UINT32_e ID,UINT8_e* cmd,UINT32_e _x,UINT32_e _y):
    EEventObject(src,ID,cmd)
{
    x = _x;
    y = _y;
}
UINT32_e ETouchEvent::getX()
{
    return x;
}
UINT32_e ETouchEvent::getY()
{
    return y;
}
EPoint* ETouchEvent::getPoint()
{
    return new EPoint(x,y);
}
