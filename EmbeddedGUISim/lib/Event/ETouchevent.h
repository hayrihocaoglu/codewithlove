#ifndef ETOUCHEVENT_H
#define ETOUCHEVENT_H

#include "lib/Event/EEventobject.h"

class ETouchEvent : public EEventObject
{
public:
    ETouchEvent(EObject* src,UINT32_e ID,UINT8_e* cmd,UINT32_e _x,UINT32_e _y);

public:
    UINT32_e getX();
    UINT32_e getY();
    EPoint* getPoint();
private:
    UINT32_e x;
    UINT32_e y;
};

#endif // ETOUCHEVENT_H
