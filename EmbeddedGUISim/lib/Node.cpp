
#include "List.h"

template<class T>
Node<T>::Node(T* _data) : EObject()
{
	data = _data;
}
template<class T>
Node<T>::~Node()
{
	delete data;
}
template<class T>	
Node<T>* Node<T>::clone()
{
    Node<T>* node = new Node(data);
    node->setIndex(index);
    node->setPriority(priority);
    node->setNextPtr(nextPtr);
    node->setPrevPtr(prevPtr);
    return node;
}
template<class T>
EString* Node<T>::toString()
{
    EString* str = new EString((UINT8_e*)"");
    str->concat(index);
    str->concat((UINT8_e*)",");
    str->concat(priority);
    str->concat((UINT8_e*)"\n");
    return str;
}

template<class T>
void Node<T>::setData(T* _data)
{
	data = _data;
}

template<class T>
void Node<T>::setPrevPtr(Node<T>* _prevPtr)
{
	prevPtr = _prevPtr;
}

template<class T>
void Node<T>::setNextPtr(Node<T>* _nextPtr)
{
	nextPtr = _nextPtr;
}

template<class T>
void Node<T>::setIndex(UINT32_e _index)
{
	index = _index;
}
template<class T>
void Node<T>::setPriority(UINT8_e _priority)
{
    priority = _priority;
}

template<class T>
T* Node<T>::getData()
{
	return data;
}
template<class T>
UINT32_e Node<T>::getIndex()
{
	return index;
}
template<class T>
Node<T>* Node<T>::getPrevPtr()
{
	return prevPtr;
}
template<class T>
Node<T>* Node<T>::getNextPtr()
{
	return nextPtr;
}
	
