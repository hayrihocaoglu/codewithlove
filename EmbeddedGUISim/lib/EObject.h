#ifndef EOBJECT_H
#define EOBJECT_H

#include "lib/EDefinition.h"

class EObject
{
public:
    EObject();
    virtual ~EObject();

    virtual EObject* clone() = 0;
    BOOL_e equals(EObject* obj);
};

#endif // EOBJECT_H
