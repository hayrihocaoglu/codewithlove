#ifndef __LIST_H_
#define __LIST_H_

#include "lib/EObject.h"
#include "lib/String/EString.h"

template <class T >
class Node : public EObject
{
public:
    Node(T* _data);
    ~Node();
    Node<T>* clone();
    EString* toString();

    T* getData();
    UINT32_e getIndex();
    Node<T>* getPrevPtr();
    Node<T>* getNextPtr();

    void setData(T* _data);
    void setPrevPtr(Node<T>* _prevPtr);
    void setNextPtr(Node<T>* _nextPtr);
    void setIndex(UINT32_e _index);
    void setPriority(UINT8_e _priority);

    bool operator ==(Node<T>& node)
    {
        return (data == node.getData());
    }
private:
    T* data;
    UINT32_e index;
    UINT8_e priority;
    Node<T>* prevPtr;
    Node<T>* nextPtr;
};

template <class T>
class List : public EObject
{
public:
    List();
    ~List();
    List<T>* clone();
    EString* toString();

    void addItem(T* item);
    void removeItem(T* item);
    void clear();
    bool isEmpty();

    T* getItem(UINT32_e _index);
    bool containItem(T* item);

    UINT32_e size();
private:
    Node<T>* startPtr;
    Node<T>* endPtr;
    UINT32_e count;

    bool containItem(T* item,Node<T>* crnPtr);
    Node<T>* getNode(UINT32_e _index);
};
#endif
