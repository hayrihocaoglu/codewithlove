#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "systemdefines.h"
#include <QMainWindow>
#include <QMouseEvent>
#include <QPainter>
#include <QPen>
#include <QPaintEvent>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private: //Private Objects
    QPainter* painter;

private: //Private Methods
    void InitializeObjects();

private: //Overriding Methods
    void mousePressEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent* event);

private: //Private Variables
    int y = 0;
    int x = 0;

};

#endif // MAINWINDOW_H
