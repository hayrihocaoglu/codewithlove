package com.wordparser;

import java.util.ArrayList;

import com.wordparser.languages.LanguageParser;
import com.wordparser.languages.TurkishParser;

public class WordBuilder 
{
	public static enum Languages 
	{
		TR
	};
	private Languages language;
	private LanguageParser langParser;
	
	public WordBuilder(Languages wordLanguage) 
	{
		language = wordLanguage;
		switch (language) {
		case TR:
			langParser = new TurkishParser();
			break;
		default:
			langParser = null;
			break;
		}
	}
	public void SetString(String str)
	{
		if(langParser != null)
		{
			langParser.SetString(str);			
		}
	}
	public String GetString()
	{
		if(langParser == null)
		{
			return null;
		}
		return langParser.GetString();
	}
	public ArrayList<String[]> GetParsedString()
	{
		if(langParser == null)
		{
			return null;
		}
		return langParser.GetParsedString();		
	}
}
