package com.wordparser.languages;

import java.util.ArrayList;

public class TurkishParser extends LanguageParser
{
	private final char[] vowels = {'a','e','�','i','o','�','u','�'
								  ,'A','E','I','�','O','�','U','�'};
	private final char[] consonants = {'b','B','c','C','�','�','d','D',
					  'f','F','g','G','�','�','h','H',
					  'j','J','k','K','l','L','m','M',
					  'n','N','p','P','r','R','s','S',
					  '�','�','t','T','v','V','y','Y',
					  'z','Z'};
	
	private ArrayList<Character> vowelList;
	private ArrayList<Character> consonantList;
	
	private void FillList()
	{
		if(vowelList == null)
		{
			vowelList = new ArrayList<Character>();
			consonantList = new ArrayList<Character>();
			for(int i = 0; i < vowels.length;i++)
			{
				vowelList.add(vowels[i]);
			}
			for(int i = 0; i <consonants.length;i++)
			{
				consonantList.add(consonants[i]);
			}
		}
	}
	@Override
	protected void ParseToWords()
	{
		words = text.split("\\s+");
		FillList();
	}

	@Override
	protected void ParseToSyllables() 
	{
		ParseToWords();
		for(int i =0;i < words.length;i++)
		{
			syllables.add(GetSyllable(words[i]));
		}
	}
	
	private String[] GetSyllable(String word)
	{
		int syllableCount = GetSyllableCount(word);
		String[] syllableArray = new String[syllableCount];
		int findSyllabe = 0;
		String[] tempArray;
		while(!word.equals(""))
		{
			tempArray = GetAndRemoveSyllable(word);
			word = tempArray[1];
			syllableArray[findSyllabe++] = tempArray[0];
		}
		
		return syllableArray;
	}
	
	private int GetSyllableCount(String word)
	{
		int count = 0;
		for(int i = 0;i < word.length();i++)
		{
			if(vowelList.contains(word.charAt(i)))
			{
				count++;
			}
		}		
		return count;
	}
	
	private String[] GetAndRemoveSyllable(String word)
	{
		int syllableCount = GetSyllableCount(word);
		String[] syllableArray = new String[2];
		syllableArray[0] = "";
		int index = 0;
		for(int i=0;i < word.length();i++)
		{
			if(vowelList.contains(word.charAt(i)))
			{
				index = i;
				break;
			}
		}
		if(syllableCount > 1)
		{
			if(IsConsonant(word, index + 1))
			{
				if(IsConsonant(word, index + 2))
				{
					if(IsConsonant(word, index + 3))
					{
						for(int i=0;i < index+3;i++)
						{
							syllableArray[0] += word.charAt(i) + "";
						}
						syllableArray[1] = word.substring(index + 3);							
					}
					else
					{
						for(int i=0;i < index+2;i++)
						{
							syllableArray[0] += word.charAt(i) + "";
						}
						syllableArray[1] = word.substring(index + 2); 							
					}
				}	
				else
				{
					for(int i=0;i < index+1;i++)
					{
						syllableArray[0] += word.charAt(i) + "";
					}
					syllableArray[1] = word.substring(index + 1);						
				}
			}
		}
		else
		{
			syllableArray[0] = word;
			syllableArray[1] = "";
		}
		return syllableArray; 
	}
	
	private boolean IsConsonant(String word,int index)
	{
		if(index < word.length())
		{
			return consonantList.contains(word.charAt(index));
		}
		return false;
	}
}
