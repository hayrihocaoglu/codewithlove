package com.wordparser.languages;

import java.util.ArrayList;

public abstract class LanguageParser 
{
	protected String text;
	protected ArrayList<String[]> syllables;
	protected String[] words;
	
	public LanguageParser() 
	{
		syllables = new ArrayList<String[]>();
	}
	
	public void SetString(String str) 
	{
		text = str;
	}

	public String GetString() 
	{
		return text;
	}

	public ArrayList<String[]> GetParsedString() 
	{
		ParseToSyllables();
		return syllables;
	}
	
	protected abstract void ParseToWords();
	protected abstract void ParseToSyllables();
	
}