#ifndef TURKISHPARSERCLS_H
#define TURKISHPARSERCLS_H

#include "LanguageParserCls.h"
#include "qstring.h"
#include "qlist.h"
#include <string>

class TurkishParserCls : public LanguageParserCls
{
public:
    TurkishParserCls();

protected:
    void ParseToWords();
    void ParseToSyllables();

private:
   QStringList* GetSyllable(QString word);
   int GetSyllableCount(QString word);
   QString* GetAndRemoveSyllable(QString word);
   bool IsConsonant(QString word,int index);

   QString vowels;
   QString consonants;

};

#endif // TURKISHPARSERCLS_H



