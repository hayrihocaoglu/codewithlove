#include "LanguageParserCls.h"

LanguageParserCls::LanguageParserCls()
{
    syllables = new QList<QStringList*>;
}

void LanguageParserCls::SetString(QString str)
{
    text = str;
}

QString LanguageParserCls::GetString()
{
    return text;
}

QList<QStringList*>* LanguageParserCls::GetParsedString()
{
    ParseToSyllables();
    return syllables;
}
