#ifndef WORDBUILDER_H
#define WORDBUILDER_H

#include "TurkishParserCls.h"
#include "LanguageParserCls.h"

enum Languages
{
    TR
};

class WordBuilder
{
public:
    ~WordBuilder();
    WordBuilder(Languages wordLanguage);
    void SetString(QString str);
    QString GetString();
    QList<QStringList*>* GetParsedString();

private:
    Languages language;
    LanguageParserCls* langParser;
};

#endif // WORDBUILDER_H
