#include "WordBuilder.h"

WordBuilder::WordBuilder(Languages wordLanguage)
{
    language = wordLanguage;
    switch (language) {
    case TR:
        langParser = new TurkishParserCls();
        break;
    default:
        langParser = NULL;
        break;
    }
}

WordBuilder::~WordBuilder()
{

}

void WordBuilder::SetString(QString str)
{
    if(langParser != NULL)
    {
        langParser->SetString(str);
    }
}

QString WordBuilder::GetString()
{
    if(langParser == NULL)
    {
        return NULL;
    }
    return langParser->GetString();
}

QList<QStringList*>* WordBuilder::GetParsedString()
{
    if(langParser == NULL)
    {
        return NULL;
    }
    return langParser->GetParsedString();
}
