#ifndef LANGUAGEPARSERCLS_H
#define LANGUAGEPARSERCLS_H

#include "qstring.h"
#include "qlist.h"

class LanguageParserCls
{
public:
    LanguageParserCls();
    LanguageParser();
    void SetString(QString str);
    QString GetString();
    QList<QStringList*>* GetParsedString();
    virtual void ParseToWords() = 0;
    virtual void ParseToSyllables() = 0;
protected:
    QString text;
    QList<QStringList*>* syllables;
    //QString* words;
    QStringList words;
};

#endif // LANGUAGEPARSERCLS_H
