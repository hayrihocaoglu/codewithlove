#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextDocument>
#include <QLineEdit>
#include "WordBuilder.h"
#include <QPrintDialog>
#include <QPrinter>
#include <QPrinterInfo>

class QAction;
class QMenu;
class QPlainTextEdit;
class QSessionManager;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();

    void loadFile(const QString &fileName);

protected:
    void closeEvent(QCloseEvent *event) override;

private slots:
    void newFile();
    void open();
    bool save();
    bool saveAs();
    void parseText();
    //void setTextColor();
    void about();
    void documentWasModified();    
    void fontChange();
    void print();
    void selectSeperator();

    void changeTextWithParseText(QString text);

#ifndef QT_NO_SESSIONMANAGER
    void commitData(QSessionManager &);
#endif

private:
    void createActions();
    void createStatusBar();
    void readSettings();
    void writeSettings();
    bool maybeSave();
    bool saveFile(const QString &fileName);
    void setCurrentFile(const QString &fileName);

    QString selectColor(int value);
    QString strippedName(const QString &fullFileName);

    QPlainTextEdit *textEdit;
    QString curFile;

    QString lightColors[21] = { "6666ff",
                                "ff6666",
                                "ffff66",
                                "66ff99",
                                "ffa366",
                                "ffb366",
                                "ff66b3",
                                "ff0080",
                                "ff66ff",
                                "66d9ff",
                                "d9ff66",
                                "a3c2c2",
                                "b3b3b3",
                                "ff66d9",
                                "ff6633",
                                "00cccc",
                                "00ff80",
                                "ffff00",
                                "9494b8",
                                "ff1aff",
                                "cc6699"};

    QString darkColors[21] = {"000099",
                              "990000",
                              "999900",
                              "009933",
                              "993d00",
                              "994d00",
                              "99004d",
                              "ff0055",
                              "990099",
                              "007399",
                              "739900",
                              "3d5c5c",
                              "4d4d4d",
                              "990073",
                              "cc3300",
                              "006666",
                              "00994d",
                              "ffd11a",
                              "52527a",
                              "660066",
                              "993366"};

    QList<QStringList*>* words;
    QString heceArasi = "";
    QLineEdit* seperatorLine;
    WordBuilder* wordBuilder;
    QString currentText;
};

#endif
