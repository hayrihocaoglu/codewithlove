#include <QtWidgets>

#include "mainwindow.h"


MainWindow::MainWindow()
    : textEdit(new QPlainTextEdit)
{
    wordBuilder = new WordBuilder(TR);
    setCentralWidget(textEdit);
    seperatorLine = new QLineEdit();

    qsrand((uint)QTime::currentTime().msec());
    createActions();
    createStatusBar();

    readSettings();

    connect(textEdit->document(), &QTextDocument::contentsChanged,
            this, &MainWindow::documentWasModified);

#ifndef QT_NO_SESSIONMANAGER
    QGuiApplication::setFallbackSessionManagementEnabled(false);
    connect(qApp, &QGuiApplication::commitDataRequest,
            this, &MainWindow::commitData);
#endif

    setCurrentFile(QString());
    setUnifiedTitleAndToolBarOnMac(true);

}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (maybeSave()) {
        writeSettings();
        event->accept();
    } else {
        event->ignore();
    }
}

void MainWindow::newFile()
{
    if (maybeSave()) {
        textEdit->clear();
        setCurrentFile(QString());
    }
}

void MainWindow::open()
{
    if (maybeSave()) {
        QString fileName = QFileDialog::getOpenFileName(this);
        if (!fileName.isEmpty())
            loadFile(fileName);
    }
}

bool MainWindow::save()
{
    if (curFile.isEmpty()) {
        return saveAs();
    } else {
        return saveFile(curFile);
    }
}

bool MainWindow::saveAs()
{
    QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    if (dialog.exec() != QDialog::Accepted)
        return false;
    return saveFile(dialog.selectedFiles().first());
}

void MainWindow::parseText()
{
    currentText =  textEdit->document()->toPlainText();
    changeTextWithParseText(currentText);
}
QString MainWindow::selectColor(int value)
{
    int number = 21;
    int randomValue = qrand() % number;
    qDebug() << randomValue;
    if(value %2 == 0)
    {
        //return darkColors[randomValue];
        return "FF0000";
    }
    else
    {
        //return lightColors[randomValue];
        return "0000FF";
    }
}

/*void MainWindow::setTextColor()
{
    QColor color = QColorDialog::getColor(Qt::white,this,"Choose color");
    if(color.isValid())
    {
        //textEdit->
    }
}*/
void MainWindow::about()
{
   QMessageBox::about(this, tr("Uygulama Hakkında"),
            tr("Uygulamada karşılaşılan hataları ve uygulama ile ilgili \n"
               "istek ve görüşlerinizi ....... adresine bildirebilirsiniz."));
}

void MainWindow::documentWasModified()
{
    setWindowModified(textEdit->document()->isModified());
}

void MainWindow::fontChange()
{
    bool ok = false;
    QFont font = QFontDialog::getFont(&ok, QFont("Helvetica [Cronyx]", 10), this);
    if (ok)
    {
        textEdit->document()->setDefaultFont(font);
    }
}

void MainWindow::print()
{
    /*QPrinter* printer = new QPrinter(QPrinterInfo::defaultPrinter(),QPrinter::HighResolution);
    textEdit->document()->print(printer);*/
    QPrinter printer;
    QPrintDialog dialog(&printer,this);
    if(dialog.exec() == QPrintDialog::Accepted)
    {
        textEdit->document()->print(&printer);
    }
}

void MainWindow::selectSeperator()
{
    heceArasi = seperatorLine->text();
    changeTextWithParseText(currentText);
}

void MainWindow::changeTextWithParseText(QString text)
{
    QString parsedText;
    wordBuilder->SetString(text);
    words = wordBuilder->GetParsedString();
    QString html;
    for(int i=0;i<words->size();i++)
    {
        for (int j = 0; j < words->at(i)->size(); j++)
        {
            if(j == words->at(i)->size() -1)
            {
                html += QString("<span style=\" color:#" + selectColor(j) + ";\">%1</span>").arg(words->at(i)->at(j) + "");
            }
            else
            {
                html += QString("<span style=\" color:#" + selectColor(j) + ";\">%1</span>").arg(words->at(i)->at(j) + heceArasi);
            }
            parsedText += words->at(i)->at(j) + " ";
        }
        html += QString("<span style=\" color:#000000;\">%1</span>").arg(" ");
        parsedText += " ";
        words->at(i)->clear();
        delete words->at(i);
    }
    words->clear();

    textEdit->document()->setPlainText(parsedText);
    textEdit->document()->setHtml(html);
}

void MainWindow::createActions()
{

    QMenu *fileMenu = menuBar()->addMenu(tr("&Dosya"));
    QToolBar *fileToolBar = addToolBar(tr("Dosya"));

    const QIcon newIcon = QIcon::fromTheme("document-new", QIcon(":/images/new.png"));
    QAction *newAct = new QAction(newIcon, tr("&Yeni..."), this);
    newAct->setShortcuts(QKeySequence::New);
    newAct->setStatusTip(tr("Yeni Dosya"));
    connect(newAct, &QAction::triggered, this, &MainWindow::newFile);
    fileMenu->addAction(newAct);
    fileToolBar->addAction(newAct);

    const QIcon openIcon = QIcon::fromTheme("document-open", QIcon(":/images/open.png"));
    QAction *openAct = new QAction(openIcon, tr("&Aç..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing file"));
    connect(openAct, &QAction::triggered, this, &MainWindow::open);
    fileMenu->addAction(openAct);
    fileToolBar->addAction(openAct);

    const QIcon saveIcon = QIcon::fromTheme("document-save", QIcon(":/images/save.png"));
    QAction *saveAct = new QAction(saveIcon, tr("&Kaydet..."), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Dosyayı Kaydet"));
    connect(saveAct, &QAction::triggered, this, &MainWindow::save);
    fileMenu->addAction(saveAct);
    fileToolBar->addAction(saveAct);

    const QIcon saveAsIcon = QIcon::fromTheme("document-save-as",QIcon(":/images/save_as.png"));
    QAction *saveAsAct = fileMenu->addAction(saveAsIcon, tr("Farklı &Kaydet..."), this, &MainWindow::saveAs);
    saveAsAct->setShortcuts(QKeySequence::SaveAs);
    saveAsAct->setStatusTip(tr("Dosyayı Farklı Kaydet"));

    const QIcon parseIcon = QIcon::fromTheme("document-parse",QIcon(":/images/parse.png"));
    QAction *parseAct = new QAction(parseIcon, tr("&Hecele..."), this);
    parseAct->setStatusTip(tr("Parse the document"));
    connect(parseAct, &QAction::triggered, this, &MainWindow::parseText);
    fileMenu->addAction(parseAct);
    fileToolBar->addAction(parseAct);

    fileMenu->addSeparator();

    const QIcon exitIcon = QIcon::fromTheme("application-exit");
    QAction *exitAct = fileMenu->addAction(exitIcon, tr("&Çıkış"), this, &QWidget::close);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Uygulamadan Çık"));

    QMenu *editMenu = menuBar()->addMenu(tr("&Düzenle"));
    QToolBar *editToolBar = addToolBar(tr("Düzenle"));
#ifndef QT_NO_CLIPBOARD
    const QIcon cutIcon = QIcon::fromTheme("edit-cut", QIcon(":/images/cut.png"));
    QAction *cutAct = new QAction(cutIcon, tr("&Kes"), this);
    cutAct->setShortcuts(QKeySequence::Cut);
    cutAct->setStatusTip(tr("Panoda seçili olan içeriği kes"));
    connect(cutAct, &QAction::triggered, textEdit, &QPlainTextEdit::cut);
    editMenu->addAction(cutAct);
    editToolBar->addAction(cutAct);

    const QIcon copyIcon = QIcon::fromTheme("edit-copy", QIcon(":/images/copy.png"));
    QAction *copyAct = new QAction(copyIcon, tr("&Kopyala"), this);
    copyAct->setShortcuts(QKeySequence::Copy);
    copyAct->setStatusTip(tr("Panoda seçili olan içeriği kopyala"));
    connect(copyAct, &QAction::triggered, textEdit, &QPlainTextEdit::copy);
    editMenu->addAction(copyAct);
    editToolBar->addAction(copyAct);

    const QIcon pasteIcon = QIcon::fromTheme("edit-paste", QIcon(":/images/paste.png"));
    QAction *pasteAct = new QAction(pasteIcon, tr("&Yapıştır"), this);
    pasteAct->setShortcuts(QKeySequence::Paste);
    pasteAct->setStatusTip(tr("Panoda seçili olan içeriği yapıştır"));
    connect(pasteAct, &QAction::triggered, textEdit, &QPlainTextEdit::paste);
    editMenu->addAction(pasteAct);
    editToolBar->addAction(pasteAct);

    const QIcon printIcon = QIcon::fromTheme("edit-print", QIcon(":/images/print.png"));
    QAction *printAct = new QAction(printIcon, tr("&Yazdır"), this);
    printAct->setStatusTip(tr("Dosyayı Yazdır"));
    connect(printAct, &QAction::triggered, this, &MainWindow::print);
    editMenu->addAction(printAct);
    fileToolBar->addAction(printAct);

    const QIcon fontIcon = QIcon::fromTheme("edit-font", QIcon(":/images/font.png"));
    QAction *fontAct = new QAction(fontIcon, tr("Font"), this);
    fontAct->setStatusTip(tr("Font Ayarı"));
    connect(fontAct, &QAction::triggered, this, &MainWindow::fontChange);
    editMenu->addAction(fontAct);
    fileToolBar->addAction(fontAct);

    menuBar()->addSeparator();

#endif // !QT_NO_CLIPBOARD

    QMenu* helpMenu = menuBar()->addMenu(tr("&Hakkında"));
    const QIcon aboutIcon = QIcon::fromTheme("edit-about", QIcon(":/images/about.png"));
    QAction* aboutAct = helpMenu->addAction(aboutIcon, tr("Hakkında"), this, &MainWindow::about);
    aboutAct->setStatusTip(tr("Uygulama Hakkında"));


#ifndef QT_NO_CLIPBOARD
    cutAct->setEnabled(false);
    copyAct->setEnabled(false);
    connect(textEdit, &QPlainTextEdit::copyAvailable, cutAct, &QAction::setEnabled);
    connect(textEdit, &QPlainTextEdit::copyAvailable, copyAct, &QAction::setEnabled);

#endif // !QT_NO_CLIPBOARD


    seperatorLine->setMaxLength(5);
    seperatorLine->setMaximumWidth(40);
    editToolBar->addWidget(seperatorLine);

    const QIcon selectSeperatorIcon = QIcon::fromTheme("select", QIcon(":/images/select.png"));
    QAction *selectSepAct = new QAction(selectSeperatorIcon, tr("Seç"), this);
    selectSepAct->setStatusTip(tr("Ayraç Seç"));
    connect(selectSepAct, &QAction::triggered, this, &MainWindow::selectSeperator);
    editToolBar->addAction(selectSepAct);
    editToolBar->addSeparator();

}

void MainWindow::createStatusBar()
{
    statusBar()->showMessage(tr("Hazır"));
}

void MainWindow::readSettings()
{
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
    const QByteArray geometry = settings.value("geometry", QByteArray()).toByteArray();
    if (geometry.isEmpty()) {
        const QRect availableGeometry = QApplication::desktop()->availableGeometry(this);
        resize(availableGeometry.width() / 3, availableGeometry.height() / 2);
        move((availableGeometry.width() - width()) / 2,
             (availableGeometry.height() - height()) / 2);
    } else {
        restoreGeometry(geometry);
    }
}

void MainWindow::writeSettings()
{
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
    settings.setValue("geometry", saveGeometry());
}

bool MainWindow::maybeSave()
{
    if (!textEdit->document()->isModified())
        return true;
    const QMessageBox::StandardButton ret
        = QMessageBox::warning(this, tr("Uygulama"),
                               tr("Dosya değiştirildi.\n"
                                  "Değişiklikleri kaydetmek ister misiniz?"),
                               QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    switch (ret) {
    case QMessageBox::Save:
        return save();
    case QMessageBox::Cancel:
        return false;
    default:
        break;
    }
    return true;
}

void MainWindow::loadFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Uygulama"),
                             tr("Dosya açılamadı %1:\n%2.")
                             .arg(QDir::toNativeSeparators(fileName), file.errorString()));
        return;
    }

    QTextStream in(&file);
#ifndef QT_NO_CURSOR
    QApplication::setOverrideCursor(Qt::WaitCursor);
#endif
    textEdit->setPlainText(in.readAll());
#ifndef QT_NO_CURSOR
    QApplication::restoreOverrideCursor();
#endif

    setCurrentFile(fileName);
    statusBar()->showMessage(tr("Dosya yüklendi"), 2000);
}

bool MainWindow::saveFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Uygulama"),
                             tr("Dosya kaydedilemedi %1:\n%2.")
                             .arg(QDir::toNativeSeparators(fileName),
                                  file.errorString()));
        return false;
    }

    QTextStream out(&file);
#ifndef QT_NO_CURSOR
    QApplication::setOverrideCursor(Qt::WaitCursor);
#endif
    out << textEdit->toPlainText();
#ifndef QT_NO_CURSOR
    QApplication::restoreOverrideCursor();
#endif

    setCurrentFile(fileName);
    statusBar()->showMessage(tr("Dosya kaydedildi"), 2000);
    return true;
}

void MainWindow::setCurrentFile(const QString &fileName)
{
    curFile = fileName;
    textEdit->document()->setModified(false);
    setWindowModified(false);

    QString shownName = curFile;
    if (curFile.isEmpty())
        shownName = "Heceleme Uygulaması";
    setWindowFilePath(shownName);
}

QString MainWindow::strippedName(const QString &fullFileName)
{
    return QFileInfo(fullFileName).fileName();
}
#ifndef QT_NO_SESSIONMANAGER
void MainWindow::commitData(QSessionManager &manager)
{
    if (manager.allowsInteraction()) {
        if (!maybeSave())
            manager.cancel();
    } else {
        // Non-interactive: save without asking
        if (textEdit->document()->isModified())
            save();
    }
}
#endif
