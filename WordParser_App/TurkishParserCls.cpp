#include "TurkishParserCls.h"

TurkishParserCls::TurkishParserCls()
{
    vowels = "aAeEıIoOöÖuUüÜiİ";

    consonants = "bBcCçÇdDfFgGğĞhHjJkKlLmMnNpPrRsSşŞtTvVyYzZ";

}

void TurkishParserCls::ParseToWords()
{
    words = text.split(QRegExp("[ ]"), QString::SkipEmptyParts);
}

void TurkishParserCls::ParseToSyllables()
{
    QStringList* syllableArray;
    ParseToWords();
    for(int i =0;i < words.size();i++)
    {
        syllableArray = GetSyllable(words.at(i));
        syllables->append(syllableArray);
    }
}

QStringList* TurkishParserCls::GetSyllable(QString word)
{
    QStringList* syllableArray = new QStringList();
    QString* tempArray;
    word.replace('\n',"<br>");
    while(word.compare("") != 0)
    {
        tempArray = GetAndRemoveSyllable(word);
        word = tempArray[1];
        syllableArray->append(tempArray[0]);
    }

    return syllableArray;
}

int TurkishParserCls::GetSyllableCount(QString word)
{
    int count = 0;
    for(int i = 0;i < word.length();i++)
    {
        if(vowels.contains(word.at(i)))
        {
            count++;
        }
    }
    return count;
}

QString* TurkishParserCls::GetAndRemoveSyllable(QString word)
{
    int syllableCount = GetSyllableCount(word);
    QString* syllableArray = new QString[2];
    syllableArray[0] = "";
    int index = 0;
    for(int i=0;i < word.length();i++)
    {
        if(vowels.contains(word.at(i)))
        {
            index = i;
            break;
        }
    }
    if(syllableCount > 1)
    {
        if(IsConsonant(word, index + 1))
        {
            if(IsConsonant(word, index + 2))
            {
                if(IsConsonant(word, index + 3))
                {
                    for(int i=0;i < index+3;i++)
                    {
                        syllableArray[0] = syllableArray[0] + word.at(i) + "";
                    }
                    syllableArray[1] = word.mid(index + 3); //word.(index + 3);
                }
                else
                {
                    for(int i=0;i < index+2;i++)
                    {
                        syllableArray[0] = syllableArray[0] + word.at(i) + "";
                    }
                    syllableArray[1] = word.mid(index + 2);
                }
            }
            else
            {
                for(int i=0;i < index+1;i++)
                {
                    syllableArray[0] = syllableArray[0] + word.at(i) + "";
                }
                syllableArray[1] = word.mid(index + 1);
            }
        }
        else
        {
            for(int i=0;i < index+1;i++)
            {
                syllableArray[0] = syllableArray[0] + word.at(i) + "";
            }
            syllableArray[1] = word.mid(index + 1);
        }
    }
    else
    {
        syllableArray[0] = word;
        syllableArray[1] = "";
    }
    return syllableArray;
}

bool TurkishParserCls::IsConsonant(QString word,int index)
{
    if(index < word.length())
    {
        return consonants.contains(word.at(index));
    }
    return false;
}

