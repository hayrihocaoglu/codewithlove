#include "WordBuilder.h"
#include <QApplication>
#include "QDebug"

#include <QCommandLineParser>
#include <QCommandLineOption>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
    /*QList<QStringList*>* words;
    WordBuilder* wordBuilder = new WordBuilder(TR);
    wordBuilder->SetString("Mor sümbülüm benim");
    words = wordBuilder->GetParsedString();
    for(int i=0;i<words->size();i++)
    {
        for (int j = 0; j < words->at(i)->size(); j++)
        {
            qDebug() << words->at(i)->at(j) + " ";
        }
        qDebug() << "\n";
    }
    while(true);*/


    Q_INIT_RESOURCE(application);

    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("QtProject");
    QCoreApplication::setApplicationName("Application Example");
    QCoreApplication::setApplicationVersion(QT_VERSION_STR);
    QCommandLineParser parser;
    parser.setApplicationDescription(QCoreApplication::applicationName());
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("file", "The file to open.");
    parser.process(app);

    MainWindow mainWin;
    if (!parser.positionalArguments().isEmpty())
       mainWin.loadFile(parser.positionalArguments().first());
    mainWin.show();
    return app.exec();  

}
